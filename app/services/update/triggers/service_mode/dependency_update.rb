# frozen_string_literal: true

module Update
  module Triggers
    module ServiceMode
      module DependencyUpdate
        def call
          create_update_run

          super
        ensure
          save_execution_details
        end

        private

        # Update job
        #
        # @return [Update::Job]
        def update_job
          @update_job ||= Project.find_or_initialize_by(name: project_name)
                                 .update_jobs
                                 .find_or_initialize_by(
                                   package_ecosystem: package_ecosystem,
                                   directory: directory
                                 )
        end

        # Dependency update run
        #
        # @return [Update::Run]
        def update_run
          @update_run ||= Update::Run.create!(job: update_job)
        end
        alias_method :create_update_run, :update_run

        # Persist execution errors
        #
        # @return [void]
        def save_execution_details
          update_run.update_attributes!(finished_at: Time.zone.now)
          update_run.save_errors!(UpdateFailures.fetch)
          update_run.save_log_entries!(UpdateLogs.fetch)
        end
      end
    end
  end
end
