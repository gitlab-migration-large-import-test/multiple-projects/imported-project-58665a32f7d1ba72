# frozen_string_literal: true

module Update
  module Triggers
    class NotifyRelease < ApplicationService
      using Rainbow

      def initialize(dependency_name, package_ecosystem)
        @dependency_name = dependency_name
        @package_ecosystem = package_ecosystem
      end

      def call
        errors = []

        configurations.each do |config|
          context = {
            job: "notify-release (#{config[:project_name]})",
            ecosystem: package_ecosystem,
            dependency: dependency_name
          }

          run_within_context(context) do
            Dependabot::UpdateService.call(
              dependency_name: dependency_name,
              package_ecosystem: package_ecosystem,
              directory: config[:directory],
              project_name: config[:project_name]
            )
          rescue StandardError => e
            log_error(e)
            errors << e
          end

          raise("One or more errors occurred while updating #{dependency_name} for #{package_ecosystem}") if errors.any?
        end
      end

      private

      attr_reader :dependency_name, :package_ecosystem

      # Configurations for ecosystem
      #
      # @return [Array]
      def configurations
        @configurations ||= ::Project.all.map do |project|
          configs = project.configuration&.entries(package_ecosystem: package_ecosystem)
          next if configs.blank?

          configs.map { |conf| { project_name: project.name, directory: conf[:directory] } }
        end.flatten.compact
      end
    end
  end
end
