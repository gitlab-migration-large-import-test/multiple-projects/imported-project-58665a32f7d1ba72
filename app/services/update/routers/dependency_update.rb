# frozen_string_literal: true

module Update
  module Routers
    class DependencyUpdate < ApplicationService
      include ServiceHelpersConcern

      def initialize(project_name, package_ecosystem, directory)
        @project_name = project_name
        @package_ecosystem = package_ecosystem
        @directory = directory
      end

      def call
        container_runner_class.call(
          package_ecosystem: package_ecosystem,
          task_name: "update",
          task_args: [project_name, package_ecosystem, directory]
        )
      end

      private

      attr_reader :project_name, :package_ecosystem, :directory
    end
  end
end
