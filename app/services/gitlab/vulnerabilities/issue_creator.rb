# frozen_string_literal: true

module Gitlab
  module Vulnerabilities
    class IssueCreator < ApplicationService
      using Rainbow

      include Helpers

      # Vulnerability issue creator
      #
      # @param [Project] project
      # @param [Vulnerability] vulnerability
      # @param [Dependabot::DependencyFile] dependency_file
      # @param [Array] assignees
      # @param [Boolean] confidential
      def initialize(project:, vulnerability:, dependency_file:, assignees:, confidential:)
        @project = project
        @vulnerability = vulnerability
        @dependency_file = dependency_file
        @assignees = assignees
        @confidential = confidential
      end

      def call
        return log(:info, "   vulnerability issue already exists: #{issue.web_url.bright}") if issue

        gitlab_issue = gitlab.create_issue(
          project_name,
          vulnerability.summary,
          description: issue_body,
          labels: issue_labels,
          confidential: confidential,
          **assignees_option
        )
        log(:info, "  created security vulnerability issue: #{gitlab_issue.web_url.bright}")
        return gitlab_issue if standalone?

        VulnerabilityIssue.create(
          iid: gitlab_issue.iid,
          project: project,
          directory: directory,
          package: vulnerability.package,
          package_ecosystem: vulnerability.package_ecosystem,
          vulnerability: vulnerability,
          web_url: gitlab_issue.web_url
        )
      end

      private

      attr_reader :project, :vulnerability, :dependency_file, :assignees, :confidential

      delegate :directory, to: :dependency_file
      delegate :standalone?, to: AppConfig

      # Project name for issue creation
      #
      # @return [String]
      def project_name
        @project_name ||= project.forked_from_name || project.name
      end

      # Assignee id's
      #
      # @return [Array]
      def assignee_ids
        @assignee_ids ||= UserFinder.call(assignees)
      end

      # Severity label
      #
      # @return [String]
      def severity_label
        @severity_label ||= "severity:#{vulnerability.severity.downcase}"
      end

      # Vulnerability issue template
      #
      # @return [String]
      def issue_body
        IssueTemplate.call(vulnerability, dependency_file)
      end

      # Existing vulnerability issue
      #
      # @return [<VulnerabilityIssue, Gitlab::ObjectifiedHash>]
      def issue
        @issue ||= if standalone?
                     gitlab.issues(
                       project_name,
                       labels: "security,#{severity_label}",
                       search: vulnerability.summary
                     ).then { |issues| issues.empty? ? nil : issues.first }
                   else
                     project.vulnerability_issues.find_by(
                       directory: directory,
                       vulnerability: vulnerability
                     )
                   end
      rescue Mongoid::Errors::DocumentNotFound
        nil
      end

      # Assignees option
      #
      # @return [Hash]
      def assignees_option
        return {} unless assignee_ids

        assignee_ids.size == 1 ? { assignee_id: assignee_ids.first } : { assignee_ids: assignee_ids }
      end

      # Issue labels
      #
      # @return [String]
      def issue_labels
        ["security", "severity:#{vulnerability.severity.downcase}"]
          .filter_map { |label| create_vulnerability_label(project_name, label) }
          .join(",")
      end
    end
  end
end
